
export const Messages = {
  FORM_CLIENTE : 'INFORMACIÓN CLIENTE',
  FORM_CLIENTE_SAVE_SUCCESS : 'La Información del Cliente fue almacenada satisfactoriamente.',
  FORM_CLIENTE_DELETE_SUCCESS : 'La Información del Cliente fue eliminada satisfactoriamente.',
  FORM_CUENTAS_BANCARIAS : 'INFORMACIÓN CUENTAS',
  FORM_CUENTAS_BANCARIAS_SAVE_SUCCESS : 'La cuenta bancaria, fue almacenada satisfactoriamente.',
  FORM_CUENTAS_BANCARIAS_DELETE_SUCCESS : 'La cuenta bancaria, fue eliminada satisfactoriamente.',

  FORM_PRESTAMOS : 'INFORMACIÓN PRESTAMOS',
  FORM_PRESTAMO_SAVE_SUCCESS : 'La Información del prestamo fue almacenado satisfactoriamente.',

  FORM_ABONO : 'INFORMACIÓN ABONO',
  FORM_ABONO_SAVE_SUCCESS : 'La Información del abono fue almacenada satisfactoriamente.',
  FORM_ABONO_DELETE_SUCCESS : 'La Información del Abono fue eliminada satisfactoriamente.',
  
  FORM_LIQUIDACION : 'INFORMACIÓN LIQUIDACIÓN',
  FORM_LIQUIDACION_SUCCESS : 'La Información de la liquidación se genero satisfactoriamente.',
}