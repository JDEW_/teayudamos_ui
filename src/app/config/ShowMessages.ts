import { Injectable } from '@angular/core';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';


@Injectable({
  providedIn: 'root'
})
export class ShowMessages {
   timeMessage= 7000;

  constructor(private toastrService: NbToastrService) {
  }


  showSuccessMessages(title: String, message: String) {
    this.toastrService.show(
      message,
      title,
      { position: NbGlobalLogicalPosition.BOTTOM_END, status: NbToastStatus.SUCCESS, duration: this.timeMessage } as any
    )
  }

  showWarningMessages(title: String, message: String) {
    this.toastrService.show(
      message,
      title,
      { position: NbGlobalLogicalPosition.BOTTOM_END, status: NbToastStatus.WARNING, duration: this.timeMessage } as any
    )
  }

}