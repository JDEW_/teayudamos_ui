
export const Constants = {
    PO_INTERES : '10',
    REGION : 'es-ES',
    CURRENCY : 'COP',
    PERCENT : '%',
    MAX_DIAS_PAGO : 5,
    FORMAT_DATE_LIST: 'dd/MM/yyyy',
    ENDPOINT_API : 'http://localhost:5000/',
    // ENDPOINT_API : 'http://teayudamosenv-env.qxpppawxud.us-east-2.elasticbeanstalk.com/',
    
    EXPRESSION_REGULAR_EMAIL : ".+@.+\..+",
    EXPRESSION_REGULAR_SOLO_NUMEROS : "0|[1-9][0-9]*",

    API_SERVICE_AUTHENTICATION_SIGNIN : 'security/auth/v1/signin',
    API_SERVICE_AUTHENTICATION_SIGNUP : 'security/auth/v1/signup',

    API_SERVICE_CLIENTE_LIST : 'clientes/v1/findAll',
    API_SERVICE_CLIENTE_BY_ID : 'clientes/v1/getById/',
    API_SERVICE_CLIENTE_BY_IDENTIFICACION : 'clientes/v1/getByIdentificacion/{tipoDoc}/{cedula}',
    API_SERVICE_CLIENTE_BY_ID_DELETE : 'clientes/v1/deleteById/',
    API_SERVICE_CLIENTE_CREATE : 'clientes/v1/create',
    API_SERVICE_CLIENTE_UPDATE : 'clientes/v1/update',
    
    API_SERVICE_SOCIO_LIST : 'socios/v1/findAll',
    
    API_SERVICE_GENERIC_TIPOS_DOC_LIST : 'generic/v1/tiposDoc',
    API_SERVICE_GENERIC_MEDIOS_PAGO_LIST : 'generic/v1/mediosPago',
    API_SERVICE_GENERIC_BANCOS_LIST : 'generic/v1/bancos',

    API_SERVICE_CUENTA_BY_CLIENTE : 'cuentasbancarias/v1/findByCuentas/{idCliente}',
    API_SERVICE_CUENTA_LIST : 'cuentasbancarias/v1/findAll',
    
    API_SERVICE_CUENTA_BY_IDENTIFICACION : 'cuentasbancarias/v1/getByIdentificacion/{tipoDoc}/{cedula}',
    API_SERVICE_CUENTA_BY_ID_DELETE : 'cuentasbancarias/v1/deleteById/',
    API_SERVICE_CUENTA_CREATE : 'cuentasbancarias/v1/create',
    API_SERVICE_CUENTA_UPDATE : 'cuentasbancarias/v1/update',

    API_SERVICE_PRESTAMO_LIST : 'prestamos/v1/findAll',
    API_SERVICE_PRESTAMO_CREATE : 'prestamos/v1/create',
    API_SERVICE_PRESTAMO_UPDATE : 'prestamos/v1/update',
    API_SERVICE_PRESTAMO_DELETE : 'prestamos/v1/delete/',
    
    API_SERVICE_ABONO_BY_PRESTAMO : 'abonos/v1/getByPrestamo/{idPrestamo}',
    API_SERVICE_ABONO_BY_ID : 'abonos/v1/getAbonoDefault/',
    API_SERVICE_ABONO_CREATE : 'abonos/v1/create',
    API_SERVICE_ABONO_INACTIVE_STATE : 'abonos/v1/updateInactiveState/',

    API_SERVICE_REPORT_BALANCE : 'balance/v1/findBalance/{year}',
    API_SERVICE_REPORT_CARTERA : 'cartera/v1/findCartera/',

    API_SERVICE_REPORT_PRE_LIQUIACION : 'liquidacion/v1/findPreliquidar',
    API_SERVICE_REPORT_LIQUIACION : 'liquidacion/v1/liquidar',
}