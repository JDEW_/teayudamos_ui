/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgxLoginComponent } from './pages/login/login.component';
import { NgxRegisterComponent } from './pages/login/register/register.component';
import { NgxLogoutComponent } from './pages/login/logout/logout.component';
import { AuthGuard } from './pages/auth-guard.service';
import { NbAuthSimpleInterceptor } from './pages/login/interceptor/interceptor';
// import { registerLocaleData } from '@angular/common';
// import localeCO from '@angular/common/locales/es-CO';
// registerLocaleData(localeCO);


const PAGES_COMPONENTS = [
  AppComponent,
  NgxLoginComponent,
  NgxRegisterComponent,
  NgxLogoutComponent,
];

@NgModule({
  declarations: [...PAGES_COMPONENTS],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
    AuthGuard,
    NbAuthSimpleInterceptor,
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: HTTP_INTERCEPTORS, useClass: NbAuthSimpleInterceptor, multi: true },
    // { provide: LOCALE_ID, useValue: 'es-CO' },
  ],

})
export class AppModule {
}