export class Abono {
  codigo: number
  interes: number;
  cuota: number;
  valor: number;
  interesPagado: number;
  valorPagado: number;
  codigoPrestamo: string;
  totalPrestamo: number;
  codigoMedioPago: string;
  nombreCliente: string;
  estado: string;
  totalRestante: number;
  totalAbonado: number;
  vlrCuotaSugerida: number;
  fechaAbono: Date;

  constructor(){
  }
}