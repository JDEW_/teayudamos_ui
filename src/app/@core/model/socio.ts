export class Socio {
    tipoDoc: string;
    cedula: string;
    celular: string;
    email: string;
    snActivo: boolean;
    constructor(public codigo: number, public nombre: string){   
    }
}