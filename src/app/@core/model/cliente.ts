export class Cliente {
    tipoDoc: string;
    cedula: string;
    direccion: string;
    telefono: string;
    celular: string;
    email: string;
    snActivo: boolean;
    referidoId: number;
    socioId: number;
    socioNombre: string;
    telReferencia: string;
    nombreReferencia: string;

    constructor(public codigo: number, public nombre: string){
    }
}