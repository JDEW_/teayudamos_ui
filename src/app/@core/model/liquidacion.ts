import { SocioReport } from "./socioReport";
import { Egreso } from "./egreso";

export class Liquidacion {

    periodo: string;
    totalInteresPeriodo: number;
    socios: Array<SocioReport>;
    sociosTotales: Array<SocioReport>;
    egresos: Array<Egreso>;
    
    constructor(){
    }
  }