export class Cartera {
    cliente: string;
    valorRestante: number;
    valorCuota: number;
    telefonos: string;
    fechaUltimoAbono: string;
    nombreReferencia: string;
    telefonoReferencia: string;
    idPrestamo: number;
    valorPrestamo: number;

    constructor(){
    }
}