export class Prestamo {
    codigo: string;
    contrato: string;
    cuotas: number;
    cuotasCanceladas: number;
    estado: number;
    nombreEstado: string;
    feActualizacion: Date;
    feCreacion: Date;
    feEntrega: Date;
    usuarioCreacion: string;
    valor: number;
    idCliente: number;
    nombreCliente: string;
    valorCuotaInteres: number;
    valorCuota: number;
    fePago: Date;
    feMaxPago: Date;
    totalRestante: number;
    totalAbonado: number;
}