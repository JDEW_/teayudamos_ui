export class Balance {
    totalPrestado: number;
    totalPagado: number;
    totalInteres: number;
    periodo: string;
    namePeriodo: string;
    year: string;

    constructor(){
    }
}