export class SocioReport {
    idSocio: number;
    nameSocio: string;
    porcentaje: number;
    valorPoIntereses: number;
    valorIntereses: number;
    valorPagado: number;
    valorEgreso: number;
    valorliquidacion: number;
    socioDist: Array<SocioReport>;
    constructor(){
    }
}