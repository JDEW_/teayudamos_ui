export class Cuenta {
    codigo: number;
    nombre: string;
    dsCuenta: string;
    snActivo: boolean;
    snCuentaPrincipal: boolean;
    dsTipoCuenta: string;
    tipoCuenta: string;
    idUsuarioCreacion: number;
    codigoBanco: number;
    nombreBanco: string;
    idCliente: number;
    codigoCliente: number;
    nombreCliente: string


    constructor(){
    }
}