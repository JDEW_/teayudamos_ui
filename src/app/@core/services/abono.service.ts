import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Abono } from '../model/abono';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';

@Injectable({
  providedIn: 'root'
})
export class AbonoService {
  
  constructor( private http: HttpClient) {
  }

  findByPrestamo(idPrestamo : string){
    let urlServices = Constants.API_SERVICE_ABONO_BY_PRESTAMO;
    urlServices = urlServices.replace("{idPrestamo}", idPrestamo);
    return this.http
        .get<any[]>(`${Constants.ENDPOINT_API}${urlServices}`)
        .pipe(map(data => data));
  }

  findDefaultAbono(idPrestamo: string) {
    return this.http
    .get<any>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_ABONO_BY_ID}${idPrestamo}`)
    .pipe(map(data => data));
  }

  save(abono: Abono) {
    return this.http.post<Abono>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_ABONO_CREATE}`, abono);
  }
  
  updateInactiveState (abono: Abono) {
    return this.http.put<Abono>(`${Constants.ENDPOINT_API + Constants.API_SERVICE_ABONO_INACTIVE_STATE}`, abono);
  }
}