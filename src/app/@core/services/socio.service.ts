import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';


@Injectable({
  providedIn: 'root'
})
export class SocioService {
  

  constructor( private http: HttpClient) {
  }

  findAll() {
    return this.http
      .get<any[]>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_SOCIO_LIST}`)
      .pipe(map(data => data));
  }
}