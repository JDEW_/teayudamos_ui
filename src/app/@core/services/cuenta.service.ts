import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cuenta } from '../model/cuenta';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';


@Injectable({
  providedIn: 'root'
})
export class CuentaService {

  constructor( private http: HttpClient) {
  }

  findByCliente(idCliente : number){
    let urlServices = Constants.API_SERVICE_CUENTA_BY_CLIENTE;
    urlServices = urlServices.replace("{idCliente}", idCliente.toString());
    return this.http
        .get<any[]>(`${Constants.ENDPOINT_API}${urlServices}`)
        .pipe(map(data => data));
  }
  save(cuenta: Cuenta) {
    return this.http.post<Cuenta>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_CUENTA_CREATE}`, cuenta);
  }

  update(cuenta: Cuenta) {
    return this.http.put<Cuenta>(`${Constants.ENDPOINT_API + Constants.API_SERVICE_CUENTA_UPDATE}`, cuenta);
  }

  delete(codigo: number, idCliente: number) {
    let urlServices = Constants.API_SERVICE_CUENTA_BY_ID_DELETE;
    return this.http.delete<Cuenta>(`${Constants.ENDPOINT_API}${urlServices}${codigo}`);
  }
}