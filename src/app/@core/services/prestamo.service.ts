import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prestamo } from '../model/prestamo';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';

@Injectable({
  providedIn: 'root'
})
export class PrestamoService {
  
  constructor( private http: HttpClient) {
  }

  findAll() {
    return this.http
      .get<any[]>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_PRESTAMO_LIST}`)
      .pipe(map(data => data));
  }

  save(prestamo: Prestamo) {
    return this.http.post<Prestamo>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_PRESTAMO_CREATE}`, prestamo);
  }

  update(prestamo: Prestamo) {
    return this.http.put<Prestamo>(`${Constants.ENDPOINT_API + Constants.API_SERVICE_PRESTAMO_UPDATE}`, prestamo);
  }

  delete(idPrestamo: number) {
    return this.http.delete<Prestamo>(`${Constants.ENDPOINT_API + Constants.API_SERVICE_PRESTAMO_DELETE}${idPrestamo}`);
  }

  
}