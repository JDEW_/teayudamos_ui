import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';
import { Liquidacion } from '../model/liquidacion';


@Injectable({
  providedIn: 'root'
})
export class LiquidacionService {


  constructor(private http: HttpClient) {
  }

  findAll() {
    return this.http
        .get<Liquidacion>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_REPORT_PRE_LIQUIACION}`)
        .pipe(map(data => data));
  }


  liquidar(liquidacion: Liquidacion) {
    return this.http.post<Liquidacion>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_REPORT_LIQUIACION}`, liquidacion.sociosTotales);
  }
}