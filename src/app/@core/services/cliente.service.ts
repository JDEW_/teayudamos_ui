import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../model/cliente';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  

  constructor( private http: HttpClient) {
  }

  findByIdentificacion(tipoDoc : string, identificacion : string){
    let urlServices = Constants.API_SERVICE_CLIENTE_BY_IDENTIFICACION;
    urlServices = urlServices.replace("{tipoDoc}", tipoDoc);
    urlServices = urlServices.replace("{cedula}", identificacion);

    return this.http
      .get<any[]>(`${Constants.ENDPOINT_API}${urlServices}`)
      .pipe(map(data => data));
  }

  findAll() {
    return this.http
      .get<any[]>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_CLIENTE_LIST}`)
      .pipe(map(data => data));
  }

  findById(cliente: Cliente) {
    return this.http
    .get<any[]>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_CLIENTE_BY_ID}${cliente.codigo}`)
    .pipe(map(data => data));
  }

  save(cliente: Cliente) {
    return this.http.post<Cliente>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_CLIENTE_CREATE}`, cliente);
  }

  update(cliente: Cliente) {
    return this.http.put<Cliente>(`${Constants.ENDPOINT_API + Constants.API_SERVICE_CLIENTE_UPDATE}`, cliente);
  }

  delete(codigo: number) {
    return this.http.delete<Cliente>(`${Constants.ENDPOINT_API}${Constants.API_SERVICE_CLIENTE_BY_ID_DELETE}${codigo}`);
  }
}