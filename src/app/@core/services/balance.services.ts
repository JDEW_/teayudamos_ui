import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Constants } from '../../config/Constants';


@Injectable({
  providedIn: 'root'
})
export class BalanceService {
  
  constructor(private http: HttpClient) {
  }

  findAll(year: string) {
    let urlServices = Constants.API_SERVICE_REPORT_BALANCE;
    urlServices = urlServices.replace("{year}", year);
    return this.http
        .get<any[]>(`${Constants.ENDPOINT_API}${urlServices}`)
        .pipe(map(data => data));
  }
}