import { Injectable } from '@angular/core';
import { SmartTableData } from '../data/smart-table';

@Injectable()
export class SmartTableService extends SmartTableData {

  data = [{
    id: "C-8988888",
    nombre: 'Mark Vele',
    celular: '3145677676',
    socio: 'Edwin Martinez',
  }, 
  {
    id: "C-4567555",
    nombre: 'Juan Cano',
    celular: '3009876565',
    socio: 'Julian Lopez',
  },
];

  getData() {
    return this.data;
  }
}
