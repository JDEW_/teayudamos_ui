import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  
  {
    title: 'Inicio',
    icon: 'nb-home',
    link: '/pages/dashboard',
  },
  {
    title: 'ADMINISTRACIÓN',
    icon: 'nb-gear',
    children: [
      {
        title: 'Usuarios',
        link: '/pages/extra-components/calendar',
      },
      {
        title: 'Socios',
        link: '/pages/extra-components/calendar',
      },
      {
        title: 'Egresos',
        link: '/pages/extra-components/calendar',
      },
    ],
  },
  {
    title: 'PRESTAMOS',
    icon: 'nb-compose',
    children: [
      {
        title: 'Clientes',
        link: '/pages/prestamo/cliente',
      },
      {
        title: 'Prestamos',
        link: '/pages/prestamo/prestamo',              
      },
      {
        title: 'Cartera',
        link: '/pages/prestamo/cartera',
      },
    ],
  },
  {
    title: 'LIQUIDACIÓN',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Balance General',
        link: '/pages/liquidacion/balance',
      },
      {
        title: 'Liquidación',
        link: '/pages/liquidacion/liquidacion',
      },
    ],
  },
  
];