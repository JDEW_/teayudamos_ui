import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsRoutingModule, routedComponents } from './liquidacion-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LiquidacionBalanceComponent } from './balance/liquidacion-balance.component';
import { LiquidacionLiquidacionComponent } from './liquidacion/liquidacion-liquidacion.component';


@NgModule({
  imports: [
    ThemeModule,
    FormsRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    LiquidacionBalanceComponent,
    LiquidacionLiquidacionComponent,
  ],
})
export class LiquidacionModule { }