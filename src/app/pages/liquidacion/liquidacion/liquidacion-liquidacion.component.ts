import { Component, OnInit } from '@angular/core';
import { ShowMessages } from '../../../config/ShowMessages';
import { LiquidacionService } from '../../../@core/services/liquidacion.services';
import { Liquidacion } from '../../../@core/model/liquidacion';
import { NgForm } from '@angular/forms';
import { Messages } from '../../../config';


@Component({
  selector: 'ngx-liquidacion-liquidacion',
  templateUrl: './liquidacion-liquidacion.component.html',
  styleUrls: ['./liquidacion-liquidacion.component.scss']
})
export class LiquidacionLiquidacionComponent implements OnInit {

  liquidacion: Liquidacion = new Liquidacion();

  constructor(private liquidacionService: LiquidacionService,
    private showMessages: ShowMessages) { }


  ngOnInit() {
    
  }

  saveLiquidacion(){
    this.liquidacionService.liquidar(this.liquidacion).subscribe(result => {
      this.showMessages.showSuccessMessages(Messages.FORM_LIQUIDACION, Messages.FORM_LIQUIDACION_SUCCESS);
      this.liquidacion = new Liquidacion();
    });
  }

  getPreliquidacion(form: NgForm) {
    this.liquidacion = new Liquidacion();
    this.liquidacionService.findAll()
      .subscribe(
        restItems => {
          if (restItems != null) {
            this.liquidacion = restItems;
          }
        }
      )
  }


}
