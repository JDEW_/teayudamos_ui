import { Component, OnInit } from '@angular/core';
import { ShowMessages } from '../../../config/ShowMessages';
import { FormGroup, NgForm } from '@angular/forms';

import { BalanceService } from '../../../@core/services/balance.services';
import { Balance } from '../../../@core/model/balance';

@Component({
  selector: 'ngx-liquidacion-balance',
  templateUrl: './liquidacion-balance.component.html',
  styleUrls: ['./liquidacion-balance.component.scss']
})
export class LiquidacionBalanceComponent implements OnInit {

  ngForm: FormGroup;
  sevenBalance: Array<Balance> = [];
  lastBalance: Balance;
  balance: Balance = new Balance();

  constructor(private balanceService: BalanceService,
    private showMessages: ShowMessages) {
    this.ngOnInit()
  }

  ngOnInit() {
    this.sevenBalance = [];
    this.lastBalance  = new Balance();
  }

  consultarReport(form: NgForm) {
    this.getReportBalance();
  }

  getReportBalance() {
    this.sevenBalance = [];
    this.lastBalance  = new Balance();
    this.balanceService.findAll(this.balance.year)
      .subscribe(
        restItems => {
          if (restItems != null) {
            let contador = 1;
            for (let entry of restItems) {
              if (contador <= 12) {
                this.sevenBalance.push(entry);
              } else {
                this.lastBalance = entry;
              }
              contador = contador + 1;
            }
          }
        }
      )
  }
}