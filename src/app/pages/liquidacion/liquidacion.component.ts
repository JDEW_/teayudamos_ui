import { Component } from '@angular/core';

@Component({
  selector: 'ngx-liquidacion-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class LiquidacionComponent {
}