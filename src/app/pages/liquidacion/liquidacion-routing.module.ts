import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ LiquidacionComponent } from './liquidacion.component';
import { LiquidacionBalanceComponent } from './balance/liquidacion-balance.component';
import { LiquidacionLiquidacionComponent } from './liquidacion/liquidacion-liquidacion.component';

const routes: Routes = [{
  path: '',
  component: LiquidacionComponent,
  children: [
    {
      path: 'liquidacion',
      component: LiquidacionLiquidacionComponent,
    },
    {
      path: 'balance',
      component: LiquidacionBalanceComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class FormsRoutingModule {

}

export const routedComponents = [
  LiquidacionComponent,
  LiquidacionBalanceComponent,
  LiquidacionLiquidacionComponent,
];