import { Component, OnInit, Inject } from '@angular/core';
import { NbLogoutComponent, NbAuthService, NbTokenService} from '@nebular/auth';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

@Component({
  selector: 'ngx-logout',
  templateUrl: './logout.component.html',
})
export class NgxLogoutComponent extends NbLogoutComponent implements OnInit {

  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected router: Router,
    protected tokenService: NbTokenService
  ) {
    super(service, options, router);
    //this.redirectDelay = this.getConfigValue('forms.logout.redirectDelay');
    //this.strategy = this.getConfigValue('forms.logout.strategy');
  }

  ngOnInit() {
    super.ngOnInit();
  }

  logout(strategy: string): void {
    super.logout(strategy);
    this.tokenService.clear();
    this.router.navigate(['auth/login']);
  }

}



