import { Component, OnInit } from '@angular/core';
import { ShowMessages } from '../../../config/ShowMessages';
import { FormGroup, NgForm } from '@angular/forms';
import { CarteraService } from '../../../@core/services/cartera.service';
import { LocalDataSource } from 'ng2-smart-table';
import { Constants } from '../../../../app/config/Constants';

@Component({
  selector: 'ngx-prestamo-cartera',
  templateUrl: './prestamo-cartera.component.html',
  styleUrls: ['./prestamo-cartera.component.scss']
})

export class PrestamoCarteraComponent {

  source: LocalDataSource = new LocalDataSource();

  constructor(private carteraService : CarteraService,
    private showMessages: ShowMessages) {
    this.getReporteCartera();    
  }

  onCustomAction(event) {
    switch ( event.action) {
      case 'editRecord':
        this.onShowAbonos();
        break;
      }
  }

  getReporteCartera(): void {
    this.carteraService.findAll()
      .subscribe(
        restItems => {
          this.source.load(restItems);
        }
      )
  }

  onShowAbonos(){
      
  } 

  settings = {
    actions: { 
      columnTitle: 'Acciones',
      add: false,
      edit: true,
      delete: false,
      filter:true,
      class: "action-column",
      custom: [
        { name: 'editRecord', title: '<i class="nb-edit"></i>' },
    ],
      position: 'right'
    },
    columns: {
      idPrestamo: {
        title: 'Código',
        type: 'string',
      },
      cliente: {
        title: 'Cliente',
        type: 'string',
      },
      telefonos: {
        title: 'Tel',
        type: 'string',
      },
      valorRestante: {
        title: 'Valor restante',
        type: 'string',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
      valorCuota: {
        title: 'Valor cuota',
        type: 'string',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
      nombreReferencia: {
        title: 'Ref',
        type: 'string',
      },
      telefonoReferencia: {
        title: 'Tel Ref',
        type: 'string',
      },
    },
  };
}