import { Component } from '@angular/core';

@Component({
  selector: 'ngx-prestamo-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class PrestamoComponent {
}