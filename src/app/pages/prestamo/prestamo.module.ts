import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsRoutingModule, routedComponents } from './prestamo-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { PrestamoClienteCuentaComponent } from './cliente/cuenta/prestamo-cliente-cuenta.component';
import { PrestamoClienteListComponent } from './cliente/list/prestamo-cliente-list.component';
import { PrestamoPrestamoListComponent } from './prestamos/list/prestamo-prestamo-list.component';
import {NgxMaskModule, IConfig} from 'ngx-mask';

@NgModule({
  imports: [
    ThemeModule,
    FormsRoutingModule,
    Ng2SmartTableModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [
    ...routedComponents,
    PrestamoClienteCuentaComponent,
    PrestamoClienteListComponent,
    PrestamoPrestamoListComponent

  ],
})
export class PrestamoModule { }