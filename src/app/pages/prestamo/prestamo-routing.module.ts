import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ PrestamoComponent } from './prestamo.component';
import { PrestamoClienteComponent } from './cliente/prestamo-cliente.component';
import { PrestamoPrestamoComponent } from './prestamos/prestamo-prestamo.component';
import { PrestamoCarteraComponent } from '././cartera/prestamo-cartera.component';

const routes: Routes = [{
  path: '',
  component: PrestamoComponent,
  children: [
    {
      path: 'cliente',
      component: PrestamoClienteComponent,
    },
    {
      path: 'prestamo',
      component: PrestamoPrestamoComponent,
    },
    {
      path: 'cartera',
      component: PrestamoCarteraComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class FormsRoutingModule {

}

export const routedComponents = [
  PrestamoComponent,
  PrestamoClienteComponent,
  PrestamoPrestamoComponent,
  PrestamoCarteraComponent,
];