import { Component, Output, EventEmitter } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ClienteService } from '../../../../@core/services/cliente.service';
import { Cliente } from '../../../../@core/model/cliente';
import { Messages } from '../../../../config/Messages';
import { ShowMessages } from '../../../../config/ShowMessages';
import { Constants } from '../../../../config';

@Component({
  selector: 'ngx-prestamo-cliente-list',
  styleUrls: ['./prestamo-cliente-list.component.scss'],
  templateUrl: './prestamo-cliente-list.component.html',
})
export class PrestamoClienteListComponent {

  @Output() editClientePerent = new EventEmitter();

  source: LocalDataSource = new LocalDataSource();

  constructor(private clienteService :ClienteService,
            private showMessages: ShowMessages) {
    this.getClientes();    
  }

  onEditCliente(cliente: Cliente){
    this.editClientePerent.emit(cliente)
  } 

  onCustomAction(event) {
    switch ( event.action) {
      case 'editRecord':
        this.onEditCliente(event.data);
        break;
     case 'deleteRecord':
        this.deleteCliente(event.data.codigo);
    }
  }

  getClientes(): void {
    this.clienteService.findAll()
      .subscribe(
        restItems => {
          this.source.load(restItems);
        }
      )
  }

  updateListClientes(): void {
    this.clienteService.findAll()
      .subscribe(
        restItems => {
          this.source = new LocalDataSource(restItems);
        }
      )
  }
  
  deleteCliente(codigo: number): void {
    this.clienteService.delete(codigo)
      .subscribe(
        restItems => {
         this.updateListClientes()
         this.showMessages.showSuccessMessages(Messages.FORM_CLIENTE, Messages.FORM_CLIENTE_DELETE_SUCCESS);
        }, fail => {
          console.log(fail.error.message)
          this.showMessages.showWarningMessages(Messages.FORM_CLIENTE, fail.error.message);
        });
  }

  settings = {
    actions: { 
      columnTitle: 'Acciones',
      add: false,
      edit: false,
      delete: false,
      filter:true,
      class: "action-column",
      custom: [
        { name: 'editRecord', title: '<i class="nb-edit"></i>' },
        { name: 'deleteRecord', title: '<i class="nb-trash"></i>' },
      
    ],
      position: 'right'
    },
    columns: {
      cedula: {
        title: 'Identificación',
        type: 'number',
        valuePrepareFunction: (value) => { return Intl.NumberFormat('es-CO').format(value)}
      },
      nombre: {
        title: 'Nombre',
        type: 'string',
      },
      celular: {
        title: 'Celular',
        type: 'string',
      },
      socioNombre: {
        title: 'Socio',
        type: 'string',
      },
    },
  };
}