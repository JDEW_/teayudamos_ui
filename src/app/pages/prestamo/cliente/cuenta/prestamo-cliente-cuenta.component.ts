import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Constants } from '../../../../config/Constants';
import { CuentaService } from '../../../../@core/services/cuenta.service';
import { GenericService } from '../../../../@core/services/generic.service';
import { Messages } from '../../../../config/Messages';
import { ShowMessages } from '../../../../config/ShowMessages';
import { Cliente } from '../../../../@core/model/cliente';
import { Banco } from '../../../../@core/model/banco';
import { LocalDataSource } from 'ng2-smart-table';
import { Cuenta } from '../../../../@core/model/cuenta';

@Component({
  selector: 'ngx-prestamo-cliente-cuenta',
  styleUrls: ['./prestamo-cliente-cuenta.component.scss'],
  templateUrl: './prestamo-cliente-cuenta.component.html',
})

export class PrestamoClienteCuentaComponent implements OnInit {
  cliente: Cliente;

  cuenta: any = new Cuenta();
  ngForm: FormGroup;

  selectTiposCuentas: any = {};
  selectBancos: Banco[];
  constants: any = {};

  source: LocalDataSource = new LocalDataSource();
  
  constructor(private cuentaService: CuentaService, 
    private genericService :GenericService,
    private showMessages: ShowMessages) {
    this.constants = Constants;
    this.cuenta.codigoBanco = "";
    this.ngOnInit()
    
  }

  ngOnInit() {
    this.onSelectForms();
    this.onLoadTiposCuenta();
    this.getBancos();
  }

  onSelectForms(){
    this.cuenta.tipoCuenta = "";
    this.cuenta.codigoBanco = "";
    this.cuenta.nmCuenta = "";
  }

  onLoadCliente(cliente: Cliente){
    this.cliente = cliente;
    if(cliente != undefined && cliente != null){
      this.getCuentas()
    }
  }

  onLoadTiposCuenta(){
    this.selectTiposCuentas = [{'codigo': 1, 'nombre' : 'AHORROS'},
                               {'codigo': 2, 'nombre' : 'CORRIENTE'}];
  }
  
  getBancos(): void {
    this.genericService.findAllBancos()
      .subscribe(
        restItems => {
          this.selectBancos = restItems;
        }
      )
  }

  onClearSelects() {
    this.cuenta.tipoCuenta = "";
    this.cuenta.inputNCuenta = "";
    this.cuenta.codigoBanco = "";
  }

  onClearSaveForm(form: NgForm) {
    form.onReset();
    this.onClearSelects();
  }

  onClearForm() {
    // this.ngForm.clearValidators()
    this.cuenta = new Cuenta();
    this.onClearSelects();
  }

  getCuentas(): void {
    this.cuentaService.findByCliente(this.cliente.codigo)
      .subscribe(
        restItems => {
          console.log(restItems)
          this.source.load(restItems);
        }
      )
  }

  saveCuentaBancaria(form: NgForm) {
    this.cuenta.codigoCliente = this.cliente.codigo;
    this.cuentaService.save(this.cuenta)
      .subscribe(result => {
         this.cuentaService.save(this.cuenta);
       
        this.showMessages.showSuccessMessages(Messages.FORM_CUENTAS_BANCARIAS, Messages.FORM_CUENTAS_BANCARIAS_SAVE_SUCCESS);

        this.getCuentas();
      }, fail => {
        console.log(fail.error.message)
        this.showMessages.showWarningMessages(Messages.FORM_CUENTAS_BANCARIAS, fail.error.message);
      });

    
    this.onClearSaveForm(form);
  }

  deleteCuenta(codigo: number): void {
    this.cuentaService.delete(codigo, this.cliente.codigo)
      .subscribe(
        restItems => {
          this.showMessages.showSuccessMessages(Messages.FORM_CUENTAS_BANCARIAS, Messages.FORM_CUENTAS_BANCARIAS_DELETE_SUCCESS);
          this.getCuentas();
        }
      );
    
  }

  onCustomAction(event) {
    switch ( event.action) {
     case 'deleteRecord':
      this.deleteCuenta(event.data.codigo);
    }
  }

  settings = {
    actions: { 
      columnTitle: 'Acciones',
      add: false,
      edit: false,
      delete: false,
      filter:true,
      class: "action-column",
      custom: [
        { name: 'deleteRecord', title: '<i class="nb-trash"></i>' },
      
    ],
      position: 'right'
    },
    columns: {
      dsTipoCuenta: {
        title: 'Tipo cuenta',
        type: 'string',
      },
      dsCuenta: {
        title: 'Número',
        type: 'string',
      },
      nombreBanco: {
        title: 'Banco',
        type: 'string',
      },
    },
  };
}