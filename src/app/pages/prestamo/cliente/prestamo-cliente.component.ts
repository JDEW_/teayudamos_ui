import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

import { Cliente } from '../../../@core/model/cliente';
import { TipoDoc } from '../../../@core/model/tipoDoc';
import { Constants } from '../../../config/Constants';
import { Messages } from '../../../config/Messages';
import { ShowMessages } from '../../../config/ShowMessages';

import { ClienteService } from '../../../@core/services/cliente.service';
import { SocioService } from '../../../@core/services/socio.service';
import { GenericService } from '../../../@core/services/generic.service';
import { PrestamoClienteListComponent } from './list/prestamo-cliente-list.component';
import { PrestamoClienteCuentaComponent } from './cuenta/prestamo-cliente-cuenta.component';
import { Socio } from '../../../@core/model/socio';

@Component({
  selector: 'ngx-prestamo-cliente',
  styleUrls: ['./prestamo-cliente.component.scss'],
  templateUrl: './prestamo-cliente.component.html',
})
export class PrestamoClienteComponent implements OnInit {
  @ViewChild(PrestamoClienteListComponent) prestamoClienteListComponent: PrestamoClienteListComponent;
  @ViewChild(PrestamoClienteCuentaComponent) prestamoClienteCuentaComponent: PrestamoClienteCuentaComponent;

  ngForm: FormGroup;
  cliente: any = {};

  selectSocios: Socio[];
  selectReferidos: Cliente[];
  selectTiposId: TipoDoc[];
  constants: any = {};

  constructor(private clienteService: ClienteService,
    private socioService: SocioService,
    private genericService: GenericService,
    private showMessages: ShowMessages) {
    this.constants = Constants;
    this.onClearSelects()
    this.ngOnInit()
  }

  ngOnInit() {
    // Consultamos los tipos de indentificación
    this.getTiposId();
    // Consultamos los socios
    this.getSocios();
    // Consultamos los referidos
    this.getReferidos();
  }

  loadClienteInfo(cliente: Cliente) {
    this.cliente = cliente;
    if (cliente.referidoId == undefined || cliente.referidoId == null) {
      this.cliente.referidoId = "";
    }
    this.prestamoClienteCuentaComponent.onLoadCliente(cliente);
  }

  saveCliente(form: NgForm) {
    this.clienteService.save(this.cliente)
      .subscribe(result => {
        this.prestamoClienteListComponent.getClientes();
        
        this.showMessages.showSuccessMessages(Messages.FORM_CLIENTE, Messages.FORM_CLIENTE_SAVE_SUCCESS);
        this.onClearSaveForm(form);
      });
  }

  searchClienteByIdentificacion() {
    if (this.cliente.tipoDoc != null && this.cliente.tipoDoc != "" && this.cliente.cedula != null) {
      this.clienteService.findByIdentificacion(this.cliente.tipoDoc, this.cliente.cedula)
        .subscribe(
          restItems => {
            if(restItems != null){
              this.cliente = restItems;
            }
          }
        )
    }
  }

  onClearSaveForm(form: NgForm) {
    form.onReset();
    this.onClearSelects();
    this.prestamoClienteCuentaComponent.onLoadCliente(null);
  }

  onClearForm() {
    this.cliente = {};
    this.onClearSelects();
    this.prestamoClienteCuentaComponent.onLoadCliente(null);
  }

  onClearSelects() {
    this.cliente.tipoDoc = "";
    this.cliente.socioId = "";
    this.cliente.referidoId = "";
    this.cliente.codigo = null;
    this.cliente.socioNombre = null;
  }
  getReferidos(): void {
    this.clienteService.findAll()
      .subscribe(
        restItems => {
          this.selectReferidos = restItems;
        }
      )
  }

  getSocios(): void {
    this.socioService.findAll()
      .subscribe(
        restItems => {
          this.selectSocios = restItems;
        }
      )
  }

  getTiposId(): void {
    this.genericService.findAllTiposDoc()
      .subscribe(
        restItems => {
          this.selectTiposId = restItems;
        }
      )
  }
}