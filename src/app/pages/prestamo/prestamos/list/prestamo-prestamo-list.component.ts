import { Component, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { PrestamoService } from '../../../../@core/services/prestamo.service';
import { Prestamo } from '../../../../@core/model/prestamo';
import { Messages } from '../../../../config/Messages';
import { ShowMessages } from '../../../../config/ShowMessages';
import { Constants } from '../../../../config/Constants';


@Component({
  selector: 'ngx-prestamo-prestamo-list',
  styleUrls: ['./prestamo-prestamo-list.component.scss'],
  templateUrl: './prestamo-prestamo-list.component.html',
})
export class PrestamoPrestamoListComponent {
    @Output() editPrestamoPerent = new EventEmitter();
    

    source: LocalDataSource = new LocalDataSource();
    selectPrestamos: any = {};
    constants: any = {};

    constructor(private prestamoService :PrestamoService,
            private showMessages: ShowMessages) {
    
    this.ngOnInit()
  }

  onEditPrestamo(prestamo: Prestamo){
    this.editPrestamoPerent.emit(prestamo)
  } 

  ngOnInit() {
    this.getPrestamos();
  }

  onCustomAction(event) {
    switch ( event.action) {
      case 'editRecord':
        this.onEditPrestamo(event.data);
        break;
     case 'deleteRecord':
      if(event.data.estado == 0){
        this.deletePrestamo(event.data.codigo);
      }else{
        this.showMessages.showWarningMessages(Messages.FORM_PRESTAMOS, "Señor usuario solo se permiten eliminar prestamos en estado Activo y sin abonos.");
      }
    }
  }


  deletePrestamo(idPrestamo: number): void {
    this.prestamoService.delete(idPrestamo)
      .subscribe(
        restItems => {
         this.showMessages.showSuccessMessages(Messages.FORM_PRESTAMOS, "Señor usuario se ha eliminado el prestamos satisfactoriamente");
         this.getPrestamos();
        }, fail => {
          this.showMessages.showWarningMessages(Messages.FORM_PRESTAMOS, fail.error.message);
        });
  }

  getPrestamos(): void {
    this.prestamoService.findAll()
      .subscribe(
        restItems => {
          this.source.load(restItems);
        }
      )
  }

  settings = {
    actions: { 
      columnTitle: 'Acciones',
      add: false,
      edit: false,
      delete: false,
      filter:true,
      class: "action-column",
      custom: [
        { name: 'editRecord', title: '<i class="nb-edit"></i>' },
        { name: 'deleteRecord', title: '<i class="nb-trash"></i>' },
      
    ],
      position: 'right'
    },
    columns: {
      nombreCliente: {
        title: 'Cliente',
        type: 'string',
      },
      valor: {
        title: 'Valor',
        type: 'number',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
      fePago: {
        title: 'Fecha pago',
        type: 'Date',
        valuePrepareFunction: (value) => { return new DatePipe('en-US').transform(value, Constants.FORMAT_DATE_LIST); }
      },
      valorCuota: {
        title: 'Valor cuota',
        type: 'number',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
      nombreEstado: {
        title: 'Estado',
        type: 'string',
      }
    },
  };
}