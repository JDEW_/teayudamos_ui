import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Prestamo } from '../../../@core/model/prestamo';
import { PrestamoPrestamoListComponent } from './list/prestamo-prestamo-list.component';
import { PrestamoService } from '../../../@core/services/prestamo.service';
import { GenericService } from '../../../@core/services/generic.service';
import { Messages } from '../../../config/Messages';
import { ShowMessages } from '../../../config/ShowMessages';
import { Constants } from '../../../config/Constants';
import { Cliente } from '../../../@core/model/cliente';
import { MediosPago } from '../../../@core/model/mediosPago';
import { ClienteService } from '../../../@core/services/cliente.service';
import { AbonoService } from '../../../@core/services/abono.service';
import { NbDialogService } from '@nebular/theme';
import { Abono } from '../../../@core/model/abono';
import { LocalDataSource } from 'ng2-smart-table';
import { Tree } from '@angular/router/src/utils/tree';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-prestamo-prestamo',
  styleUrls: ['./prestamo-prestamo.component.scss'],
  templateUrl: './prestamo-prestamo.component.html',
})

export class PrestamoPrestamoComponent implements OnInit {
  @ViewChild(PrestamoPrestamoListComponent) prestamoPrestamoListComponent: PrestamoPrestamoListComponent;

  ngForm: FormGroup;
  prestamo: Prestamo = new Prestamo();
  abono: Abono = new Abono();

  selectClientes: Cliente[];
  selectMediosPago: MediosPago[];
  constants: any = {};
  isPrestamoSelected = false;
  isPermitAction = false;

  constructor(private prestamoService: PrestamoService,
    private clienteService: ClienteService,
    private abonoService: AbonoService,
    private genericService: GenericService,
    private dialogService: NbDialogService,
    private showMessages: ShowMessages,
    private http: HttpClient) {
    this.constants = Constants;
    this.ngOnInit()
  }

  ngOnInit() {
    this.prestamo.estado=0;
    this.prestamo.nombreEstado="ACTIVO";
    this.isPermitAction = false;
    this.getClientes();
  }

  savePrestamo(form: NgForm) {
    this.prestamoService.save(this.prestamo)
      .subscribe(result => {
        this.prestamoPrestamoListComponent.getPrestamos();
        this.showMessages.showSuccessMessages(Messages.FORM_PRESTAMOS, Messages.FORM_PRESTAMO_SAVE_SUCCESS);
        this.onClearSaveForm(form);
      });


  }

  loadPrestamoInfo(prestamo: Prestamo) {
    this.prestamo = prestamo;
    this.isPrestamoSelected = true;
    if (prestamo.codigo == undefined || prestamo.codigo == null) {
      this.prestamo.codigo = "";
      this.isPrestamoSelected = false;
    }

    if(this.prestamo.estado == 0){
      this.isPermitAction = false;
    }else{
      this.isPermitAction = true;
    }
  }


  onClearSaveForm(form: NgForm) {
    form.onReset();
    this.prestamo.estado=0;
    this.prestamo.nombreEstado="ACTIVO";
    this.isPrestamoSelected = false;
    this.isPermitAction = false;
  }

  onClearForm() {
    this.prestamo = new Prestamo();
    this.prestamo.estado=0;
    this.prestamo.nombreEstado="ACTIVO";
    this.isPrestamoSelected = false;
    this.isPermitAction = false;
  }

  getClientes(): void {
    this.clienteService.findAll()
      .subscribe(
        restItems => {
          this.selectClientes = restItems;
        }
      )
  }

  getMediosPago(): void {
    this.genericService.findAllMediosPago()
    .subscribe(
      restItems => {
        this.selectMediosPago = restItems;
      }
    )
  }

  calcularPagos() {
    if(this.prestamo.cuotas != undefined && this.prestamo.valor != undefined){
      let valorCuota = this.prestamo.valor / this.prestamo.cuotas;
      let valorCuotaInteres = this.prestamo.valor* (Number(Constants.PO_INTERES) / 100);
      this.prestamo.valorCuotaInteres = Number(Math.ceil(valorCuotaInteres));

      this.prestamo.valorCuota = Number(Math.ceil(valorCuota)) + this.prestamo.valorCuotaInteres;

    }
  }

  calcularFeMaximaPago() {
    console.log(this.prestamo)
    if(this.prestamo.fePago != undefined){
      let date: Date = this.prestamo.fePago;

      date.setDate(date.getDate() + Constants.MAX_DIAS_PAGO);
      this.prestamo.feMaxPago = date;
    }
  }


  // MANEJO DE LOS PROCESOS DE ABONOS
  source: LocalDataSource = new LocalDataSource();





  open(dialog: TemplateRef<any>) {

   if(this.prestamo.codigo != null && this.prestamo.codigo != undefined){

      this.getAbonos();
      this.getMediosPago();

      this.abono = new Abono();

      this.abonoService.findDefaultAbono(this.prestamo.codigo)
        .subscribe(result => {
          this.abono = result;
          this.abono.codigoMedioPago = "";
          this.calcularPagosAbonos();

          this.dialogService.open(dialog, { context: this.abono, hasScroll:true });

        })

    }
  }

  calcularPagosAbonos() {
    if(this.abono.valor != undefined && this.abono.valor > 0){
      let interesPagado = this.abono.totalPrestamo * (Number(Constants.PO_INTERES) / 100);
      let valorPagado = Number(this.abono.valor) - interesPagado;
      this.abono.interesPagado = Number(Math.ceil(interesPagado));
      this.abono.valorPagado = Number(Math.ceil(valorPagado));
    }else{
      this.abono.valor = 0;
      this.abono.interesPagado = 0;
      this.abono.valorPagado = 0;
    }
  }
  
  abonoSave(form: NgForm){
    if (this.abono.valor < this.abono.interesPagado){
      this.showMessages.showWarningMessages(Messages.FORM_ABONO, "Señor usuario, el valor del abono debe cubrir mínimo la cuota del interes del prestamo.");
      return;
    }
    if(this.abono.valorPagado <= this.abono.totalRestante){
      this.abonoService.save(this.abono)
      .subscribe(result => {
        this.abonoService.save
        this.showMessages.showSuccessMessages(Messages.FORM_ABONO, Messages.FORM_ABONO_SAVE_SUCCESS);
        this.getAbonos();

        this.abonoService.findDefaultAbono(this.prestamo.codigo)
          .subscribe(result => {
            this.abono.totalAbonado = result.totalAbonado;
            this.abono.totalRestante = result.totalRestante;
            this.abono.cuota = result.cuota;
            this.prestamoPrestamoListComponent.getPrestamos();
          })
      });
    }else{
      this.showMessages.showWarningMessages(Messages.FORM_ABONO, "Señor usuario, el valor del abono esta superando el total del prestamo, por favor validar el valor ingresado.");
    }
  }

  onClearAbonoSaveForm(form: NgForm) {
    form.onReset();
    this.abono.codigoMedioPago = "";
  }


  getAbonos(): void {
    this.abonoService.findByPrestamo(this.prestamo.codigo)
      .subscribe(
        restItems => {
          this.source.load(restItems);
        }
      )
  }

  updateListAbonos(): void {
    this.abonoService.findByPrestamo(this.prestamo.codigo)
      .subscribe(
        restItems => {
          this.source = new LocalDataSource(restItems);
        }
      )
  }

  inactiveAbono(abono: Abono): void {
    this.abonoService.updateInactiveState(abono)
      .subscribe(
        restItems => {
         this.updateListAbonos()
         this.showMessages.showSuccessMessages(Messages.FORM_ABONO, Messages.FORM_ABONO_DELETE_SUCCESS);
        }, fail => {
          console.log(fail.error.message)
          this.showMessages.showWarningMessages(Messages.FORM_ABONO, fail.error.message);
        });
  }

   onCustomAction(event) {
    switch (event.action) {
     case 'deleteRecord':
        if(this.prestamo.estado == 0){
          this.inactiveAbono(event.data);
        }else{
          this.showMessages.showWarningMessages(Messages.FORM_ABONO, "Señor usuario, este abono no se puede eliminar ya que el prestamo esta en estado " + this.prestamo.nombreEstado);
        }
    }
  }
  

  settings = {
    actions: { 
      columnTitle: 'Acciones',
      add: false,
      edit: false,
      delete: false,
      filter:true,
      class: "action-column",
      custom: [
        { name: 'deleteRecord', title: '<i class="nb-trash"></i>' }, 
    ],
      position: 'right'
    },
    columns: {
      valor: {
        title: 'Abono',
        type: 'number',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
      fechaAbono: {
        title: 'Fehca Abono',
        type: 'Date',
        valuePrepareFunction: (value) => { return new DatePipe('en-US').transform(value, Constants.FORMAT_DATE_LIST); }
      },
      interesPagado: {
        title: 'Pago interes',
        type: 'number',
      },
      valorPagado: {
        title: 'Pago a deuda',
        type: 'number',
        valuePrepareFunction: (value) => { return Intl.NumberFormat(Constants.REGION,{style:'currency', currency: Constants.CURRENCY}).format(value)}
      },
    },
  };
}